@echo off
setlocal enabledelayedexpansion 
echo %cd%| findstr /r ":\\Windows :\\Program Files">nul
if !errorlevel! EQU 0 (
	echo @
	echo @ Este programa no se puede ejecutar dentro de la carpeta Windows o Archivos de Programa
	echo @
	goto:eof
)

if exist "%cd%\___INDICE DE CARPETAS.txt" (
	echo @
	echo @ Este programa ya fue ejecutado en esta carpeta
	echo @
	set "reply=s"
	set /p "reply=Desea volver a ejecutar el programa? [S|N]: "
	if /i not "!reply:s=S!" == "S" goto :eof
)
echo .
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo @                       COMPRESION DE NOMBRES DE CARPETA                            @
echo @                            MAEG. Alexander Melendez                               @
echo @                                 2021 - 05 - 20                                    @
echo @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
echo .
echo                                   Instrucciones:
echo                         Ejecute este archivo en la carpeta raiz 
echo                        donde se desprenden todas las subcarpetas.
echo .
echo                                  Consideraciones:
echo  1.- El renombrado respetara los numerales en forma "1.2.3..." o "A.B.C..."
echo      siempre y cuando esten separados del nombre de la carpeta por un espacio.
echo  2.- Los nombres seran divididos por palabras siendo el limite los espacios
echo      en blanco, es decir, "carpeta 1" consiste en 2 palabras.
echo  3.- Cada parte de palabra sera reducida a sus primeras tres consonantes
echo      manteniendo la inicial, aunque sea vocal. Si la reduccion da como resultado
echo      una cadena menor a 3 caracteres, se tomaran las 3 primeras letras de la palabra.
echo.&pause
echo  4.- Si despues de la compresion el nombre colisiona con uno existente se agrega
echo      un numero aleatorio para diferenciarla de la anterior.
echo  5.- Los nombres originales de las carpetas y su compresion se registran en el 
echo      archivo ___INDICE DE CARPETAS.txt para consultas posteriores.
echo  6.- Tenga en cuenta que los nombres originales no podran ser revertidos de forma 
echo      automatizada, por lo cual el uso de esta herramienta es su responsabilidad.
set "reply=s"
set /p "reply=Esta conciente de los riesgos y acepta continuar? [S|N]: "
if /i not "!reply:s=S!" == "S" goto :eof
echo .
call:treeProcess .
goto:eof

:treeProcess
echo %~1
pushd %~1

echo INDICE PARA %cd% > "%cd%\___INDICE DE CARPETAS.txt"
for /f "delims=" %%d in ('dir /a:d /b') do (
	set "renombre="
	set "dup="
	call:CompressName "%%d" renombre
	if not exist "%~1\!renombre!" (
		ren "%%d" !renombre!
	) else (
		set "dup=!random!"
		ren "%%d" "!renombre!!dup!"
	)
	echo !renombre!!dup!		%%d >> "___INDICE DE CARPETAS.txt"
)
for /f "delims=" %%d in ('dir /a:d /b') do (
        cd "%%~fd"
        call:treeProcess "%%~fd"
	cd ..
)
popd
goto:eof

:CompressName 
set name=%1
set "name=!name:.\=!"
set name=!name:"=!
set index=0
for %%A in (!name!) do (
	set "part=%%A"
	echo !part!| findstr /r "^[[0-9] \.]+ ^[[a-z]\. [A-Z]\.]+">nul
	if !errorlevel! EQU 1 (
		call :strLen part partlen
		for %%D in ("a=A" "b=B" "c=C" "d=D" "e=E" "f=F" "g=G" "h=H" "i=I" "j=J" "k=K" "l=L" "m=M" "n=N" "¤=¥" "o=O" "p=P" "q=Q" "r=R" "s=S" "t=T" "u=U" "v=V" "w=W" "x=X" "y=Y" "z=Z" " =A" "‚=E" "¡=I" "¢=O" "£=U" "…=A" "Š=E" "=I" "•=O" "—=U" "„=A" "‰=E" "‹=I" "”=O" "=U" "µ=A" "=E" "Ö=I" "à=O" "é=U" "·=A" "Ô=E" "Þ=I" "ã=O" "ë=U" "Ž=A" "Ó=E" "Ø=I" "™=O" "š=U") do ( 
			set "part=!part:%%~D!" 
		)
		if !partlen! GTR 3 (
			set /A "substr=!partlen!-1"
			set "initial=!part:~0,1%!"
			set "rest=!part:~1!
			set "rest=!rest:A=!"
			set "rest=!rest:E=!"
			set "rest=!rest:I=!"
			set "rest=!rest:O=!"
			set "rest=!rest:U=!"
			set "newpart=!initial!!rest!"
			set "newpart=!newpart: =!"
			call :strLen newpart abbrlen
			if !abbrlen! LSS 3 ( 
				set Array[!index!]=!part:~0,3%!
			) else ( 
				set Array[!index!]=!newpart:~0,3%! 
			)
		)
		if !partlen! EQU 3 set Array[!index!]=!part!
	) else (
		set Array[!index!]=!part!
	)
	set /a "index+=1"
)
set "x=0"
set "abbr="
for /L %%C in (0,1,!index!) do (
  set "abbr=!abbr!!Array[%%C]!"
  set Array[%%C]=
)
set "abbr=!abbr: =!"
set %2=!abbr!
goto :eof

:strLen
setlocal enabledelayedexpansion

:strLen_Loop
if not "!%1:~%len%!"=="" set /A len+=1 & goto :strLen_Loop
endlocal & set %2=%len%
goto :eof
